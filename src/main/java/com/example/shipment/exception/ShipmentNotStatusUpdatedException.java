package com.example.shipment.exception;

public class ShipmentNotStatusUpdatedException extends Exception{
    public ShipmentNotStatusUpdatedException(String message){
        super(message);
    }
}
