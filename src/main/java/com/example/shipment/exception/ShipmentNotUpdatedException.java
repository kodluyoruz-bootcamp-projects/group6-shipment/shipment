package com.example.shipment.exception;

public class ShipmentNotUpdatedException extends Exception{
    public ShipmentNotUpdatedException(String message){
        super(message);
    }
}