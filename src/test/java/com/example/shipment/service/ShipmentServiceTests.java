package com.example.shipment.service;

import com.example.shipment.domain.Address;
import com.example.shipment.domain.Shipment;
import com.example.shipment.exception.*;
import com.example.shipment.repository.ShipmentRepository;
import com.example.shipment.service.ShipmentService;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;

@RunWith(MockitoJUnitRunner.class)

public class ShipmentServiceTests {
    @Mock
    ShipmentRepository shipmentRepository;
    @InjectMocks
    ShipmentService shipmentService;

    @SneakyThrows
    @Test
    public void shouldThrowShipmentNotCreatedException() {
        //Arrange
        Shipment shipment = new Shipment();
        doThrow(ShipmentNotCreatedException.class).when(shipmentRepository).insert(any(Shipment.class));
        //Act
        Throwable throwable = catchThrowable(() -> shipmentService.createShipment(shipment));
        //Assert
        assertThat(throwable).isInstanceOf(ShipmentNotCreatedException.class);
    }

    @SneakyThrows
    @Test
    public void shouldThrowShipmentNotFoundException() {
        //Arrange
        String id = "1";
        doThrow(ShipmentNotFoundException.class).when(shipmentRepository).getByTrackingCode(any(String.class));
        //Act
        Throwable throwable = catchThrowable(() -> shipmentService.getShipmentByTrackingCode(id));
        //Assert
        assertThat(throwable).isInstanceOf(ShipmentNotFoundException.class);
    }

    @SneakyThrows
    @Test
    public void shouldThrowShipmentNotStatusUpdatedException() {
        //Arrange
        String id = "1";
        String status = "yolda";
        doThrow(ShipmentNotStatusUpdatedException.class).when(shipmentRepository).updateStatus(any(String.class), any(String.class));
        //Act
        Throwable throwable = catchThrowable(() -> shipmentService.updateStatus(id, status));
        //Assert
        assertThat(throwable).isInstanceOf(ShipmentNotStatusUpdatedException.class);
    }

    @SneakyThrows
    @Test
    public void shouldThrowShipmentNotUpdatedException() {
        //Arrange
        String id = "1";
        Address address = new Address();
        doThrow(ShipmentNotUpdatedException.class).when(shipmentRepository).updateShipment(any(String.class), any(Address.class));
        //Act
        Throwable throwable = catchThrowable(() -> shipmentService.updateShipment(id, address));
        //Assert
        assertThat(throwable).isInstanceOf(ShipmentNotUpdatedException.class);
    }

    @SneakyThrows
    @Test
    public void shouldThrowShipmentNotCalculatedExceptionWhenThereAreFieldsSmallerThanZero() {
        //Arrange
        //Act
        Throwable throwable = catchThrowable(() -> shipmentService.calculatePayment(-4, 8, 8, 8));
        //Assert
        assertThat(throwable).isInstanceOf(ShipmentNotCalculatedException.class);
    }

    @SneakyThrows
    @Test
    public void shouldCalculatePayment() {
        //Arrange
        //Act
        int sut = shipmentService.calculatePayment(5, 4, 2, 10);
        //Assert
        Assert.assertEquals(sut, 40);
    }
}
